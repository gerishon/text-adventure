#!/bin/bash
# this is a text adventure game of 6 rooms in a building of the below structure. There a box in one of the rooms.

# door out
#  ^
#  |
# room1 -  room2
#   |        |
# room3	-  room4
#   |        | 
# room5 -  room6

# randomizing the box placement in one of the rooms.
ROOM_WITH_BOX=$((1 + $RANDOM % 6))

# playing instructions
HELP="You wake up in an unknown building! \n You have no memory of how you got here. \n You can head north towards the entrance, east or south to head further into the building. \n Use the directions 'east', 'west', 'north' and 'south' for your navigation. \n Oh, and there's also a box in one of the rooms, placed there randomly. \n Use 'look around' command to try and see if the box is in the room! Good luck!. \n\nToo see this message again, use 'help' command.\n\n"

#room six
roomSix() {
        echo "You are in room six! Where do you want to go..."
        read DIRECTION;
        case $DIRECTION in
                "west")
                roomFive
                ;;
                "north")
                roomFour
                ;;
                "look around")
                if [ $ROOM_WITH_BOX == 6 ]
                then
                	echo "The box is in this room!"
			cat box.txt
                else
                	echo "There's nothing here!"
                fi
		roomSix
                ;;
		"help")
                echo -e $HELP
                roomSix
		;;
                *)
                echo "Invalid action/no door in that direction!"
                roomSix;
                ;;
        esac
}

#room five
roomFive() {
        echo "You are in room five! Where do you want to go..."
        read DIRECTION;
        case $DIRECTION in
                "east")
                roomSix
                ;;
                "north")
                roomThree
                ;;
                "look around")
                if [ $ROOM_WITH_BOX == 5 ]
                then
                	echo "The box is in this room!"
			cat box.txt
                else
                	echo "There's nothing here!"
                fi
		roomFive
                ;;
		"help")
                echo -e $HELP
                roomFive
		;;
                *)
                echo "Invalid action/no door in that direction!"
                roomFive;
                ;;
        esac
}

#room four
roomFour() {
        echo "You are in room four! Where do you want to go..."
        read DIRECTION;
        case $DIRECTION in
                "west")
                roomThree
                ;;
                "north")
                roomTwo
                ;;
				 "south")
                roomSix
                ;;
                "look around")
                if [ $ROOM_WITH_BOX == 4 ]
              	then
            		echo "The box is in this room!"
			cat box.txt
                else
                	echo "There's nothing here!"
                fi
		roomFour
                ;;
		"help")
                echo -e $HELP
                roomFour
		;;
                *)
                echo "Invalid action/no door in that direction!"
                roomFour;
                ;;
        esac
}

#room three
roomThree() {
        echo "You are in room three! Where do you want to go..."
        read DIRECTION;
        case $DIRECTION in
                "east")
                roomFour
                ;;
                "north")
                roomOne
                ;;
				 "south")
                roomFive
                ;;
                "look around")
                if [ $ROOM_WITH_BOX == 3 ]
   	        then
                	echo "The box is in this room!"
			cat box.txt
                else
                	 echo "There's nothing here!"
                fi
		roomThree
                ;;
		"help")
                echo $HELP
                roomThree
		;;
                *)
                echo "Invalid action/no door in that direction!"
                roomThree;
                ;;
        esac
}

#room two
roomTwo() {
        echo "You are in room two! Where do you want to go..."
        read DIRECTION;
        case $DIRECTION in
                "west")
                roomOne
                ;;
                "south")
                roomFour
                ;;
                "look around")
                if [ $ROOM_WITH_BOX == 2 ]
                then
			echo "The box is in this room!"
			cat box.txt
                else
                        echo "There's nothing here!"
                fi
		roomTwo
                ;;
		"help")
                echo -e $HELP
                roomTwo
		;;
                *)
                echo "Invalid action/no door in that direction!"
                roomTwo;
                ;;
        esac
}

#room one
roomOne() {
	echo "You are in room one! Where do you want to go..."
	read DIRECTION;
	case $DIRECTION in
		"east")
    		roomTwo
    		;;
		"south")
    		roomThree
    		;;
		"north")
    		echo "You have found your way out of the building!"
			exit
    		;;
		"look around")
		if [ $ROOM_WITH_BOX == 1 ] 
		then
			echo "The box is in this room!"
			cat box.txt
		else
			echo "There's nothing here!"
		fi
		roomOne
    		;;
		"help")
                echo -e $HELP
		roomOne
		;;
 		*)
    		echo "Invalid action/no door in that direction!"
		roomOne
    		;;
	esac
}

# starting the game
echo -e $HELP
roomOne
